import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AppSettings } from './app.settings';

describe('AppSettings', () => {
  let component: AppSettings;
  let fixture: ComponentFixture<AppSettings>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AppSettings ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AppSettings);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });


});
