import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';


import { TemplateComponent } from './template/template.component';


const routes: Routes = [
   { path : '', redirectTo: 'login', pathMatch: 'full' },
   { path : 'home' , component : TemplateComponent},
   
  // { path : 'app-parts', component : PartsComponent}
];

@NgModule({
  imports: [ RouterModule.forRoot(routes,{useHash: true}) ],
  exports: [ RouterModule ],
  declarations: []
})
export class AppRoutingModule { }

