import { TestBed, async } from '@angular/core/testing';
import { TemplateComponent } from './template.component';
import { FormsModule } from '@angular/forms';
import { NgModule,NO_ERRORS_SCHEMA,CUSTOM_ELEMENTS_SCHEMA, Component } from '@angular/core';

import { ReactiveFormsModule } from '@angular/forms';

import { RouterTestingModule } from '@angular/router/testing';

import { url } from 'inspector';


describe('AppComponent', () => {
  let component: TemplateComponent;
  
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [RouterTestingModule,FormsModule,ReactiveFormsModule],
      declarations: [
        TemplateComponent
        
      ],
      providers : [],
      schemas: [NO_ERRORS_SCHEMA, CUSTOM_ELEMENTS_SCHEMA ],
    }).compileComponents();
  }));



  // it('load javascrip file',()=>{
  //     let URL = "assets/resource/jquery.app.js";
  //     component.loadScript(URL);
  //     component.script.src = URL;
  //     expect(component.body.appendChild).toEqual(URL);

  // });
    

});
