import { Component, OnInit  } from '@angular/core';
import { GlobalService } from '../services/global.service';
import {AppSettings} from '../constant/app.settings';
import { Router ,ActivatedRoute} from '@angular/router';


@Component({
  selector: 'app-root',
  templateUrl: './template.component.html',
  styleUrls: ['./template.component.scss']
})

export class TemplateComponent implements OnInit {

  constructor() { }
  
  ngOnInit() {}

}
