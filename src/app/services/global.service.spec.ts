import { TestBed, inject } from '@angular/core/testing';
import { GlobalService } from './global.service';
import {RequestOptions, Request, Headers, Http,HttpModule } from '@angular/http';
import { RouterTestingModule } from '@angular/router/testing';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';

describe('GlobalService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      imports:[HttpModule,RouterTestingModule],
      declarations:[],
      providers: [GlobalService,Ng4LoadingSpinnerService],
    });
  });

  it('should be created', inject([GlobalService], (service: GlobalService) => {
    expect(service).toBeTruthy();
  }));


});
