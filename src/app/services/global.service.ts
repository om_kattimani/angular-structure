import {catchError, map} from 'rxjs/operators';
import {Injectable} from '@angular/core';

import { HttpHeaders } from '@angular/common/http';
import {RequestOptions, Request, Headers, Http,HttpModule } from '@angular/http';
import {Observable} from 'rxjs';
import {AppSettings} from '../constant/app.settings';
import 'rxjs/Rx';
import 'rxjs/add/operator/map';


import { Router } from '@angular/router';

import { Response } from '@angular/http';

@Injectable({
  providedIn: 'root'
})
  /*
    Class : GlobalService 
    Created by : Nagaraj K
    Date : 12th Sep 2018
    Description : intract with rest api calls and transfer data to componets
  */
export class GlobalService {
  
   handleError; 

   constructor(private http_: Http , private router : Router) {


   }
  



  /*
    function : getPOSTData 
    variable : URL , Data
    Created by : Nagaraj K
    Date : 12th Sep 2018
    Description : used POST method to fetch the data return the JSON response to the called function
    
  */    
   
  getPOSTData(URL,Data): Observable<any> {


  
        //refresh_token=fdb8fdbecf1d03ce5e6125c067733c0d51de209c&grant_type=refresh_token
        let headers = new Headers({'Content-type': 'application/json', 'Authorization': 'Bearer '+  AppSettings.access_token});
        let options = new RequestOptions({ headers: headers });

        return this.http_.post(`${AppSettings.APP_URL}`+URL,Data,options).map((res: Response ) => {
              if (res) {
                
                if (res.status === 201) {
                    return res.json()
                }
                else if (res.status === 200) {
                    return res.json()
            }
          }
        }).catch((error: any) => {


          if (error.status < 400 ||  error.status ===500 || error.status === 401) {

              return Observable.throw(new Error(error.status));
          }
      });
      
  }
  
  /*
    function : getData 
    variables : URL , Data
    Created by : Nagaraj K
    Date : 18th Sep 2018
    Description : featch the data from given API return the result
    
  */
  getData(URL,Data): Observable<any> {


    let headers = new Headers({'Content-type': 'application/json', 'Authorization': 'Bearer '+  AppSettings.access_token});
   
    return this.http_.get(`${AppSettings.APP_URL}`+URL+Data,{
      headers: headers
     }).map((res: Response ) => {
            if (res) {
              
              if (res.status === 201) {
                  return res.json()
              }
              else if (res.status === 200) {
                  return res.json()
          }
        }
      }).catch((error: any) => {

        if (error.status < 400 ||  error.status === 500 || error.status === 401) {
            return Observable.throw(new Error(error.status));
        }
      });
  
  }


}

